package eu.endcrystats.stats.file;

import org.bukkit.plugin.Plugin;

import eu.endcrystats.stats.file.impl.DatabaseFile;

public class ConfigurationFiles {

	private DatabaseFile databaseFile;
	
	public ConfigurationFiles(Plugin plugin) {
		setDatabaseFile(new DatabaseFile(plugin));
	}

	public DatabaseFile getDatabaseFile() {
		return databaseFile;
	}

	public void setDatabaseFile(DatabaseFile databaseFile) {
		this.databaseFile = databaseFile;
	}
	
}
