package eu.endcrystats.stats.file.impl;

import org.bukkit.plugin.Plugin;

import eu.endcrystats.stats.file.AbstractFile;

public class DatabaseFile extends AbstractFile{
	
	/**
	 * Field
	 */
	private final String mysqlField = "MySQL"; 
	
	
	/**
	 * Construcotr
	 * @param plugin
	 */

	public DatabaseFile(Plugin plugin) {
		super(plugin, "database");
	}

	@Override
	public void onCreate() {
		this.getConfiguration().set(mysqlField.concat(".Host"), "localhost");
		this.getConfiguration().set(mysqlField.concat(".Port"), 3306);
		this.getConfiguration().set(mysqlField.concat(".User"), "user");
		this.getConfiguration().set(mysqlField.concat(".Password"), "password");
	}
	
	public String getHost(String field){
		return this.getConfiguration().getString(field.concat(".Host"));
	}
	
	public int getPort(String field){
		return this.getConfiguration().getInt(field.concat(".Port"));
	}
	
	public String getUser(String field){
		return this.getConfiguration().getString(field.concat(".User"));
	}
	
	public String getPassword(String field){
		return this.getConfiguration().getString(field.concat(".Password"));
	}
	
	public String getMysqlField() {
		return mysqlField;
	}

}
