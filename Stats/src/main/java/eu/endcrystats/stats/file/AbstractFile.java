package eu.endcrystats.stats.file;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import lombok.Getter;
@Getter
public abstract class AbstractFile {
	
	private File file;
	private FileConfiguration configuration;
	
	public AbstractFile(Plugin plugin, String fileName) {
	    this.file = new File(plugin.getDataFolder(), fileName.concat(".yml"));
	    this.configuration = YamlConfiguration.loadConfiguration(this.file);
	    write();
	}
	
	private void write(){
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			onCreate();
			save();
		}
	}
	
	public void save(){
		try {
			this.configuration.save(this.file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public abstract void onCreate();
	
	
	public File getFile() {
		return file;
	}
	
	public FileConfiguration getConfiguration() {
		return configuration;
	}
	
	

}
