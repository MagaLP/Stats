package eu.endcrystats.stats.database.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import eu.endcrystats.stats.database.Credential;
import eu.endcrystats.stats.database.DatabaseConnector;

public class MySQLConnector extends DatabaseConnector{
	
	public final String prefix = "jdbc:mysql://";
	public final String driver = "com.mysql.jdbc.Driver";
	public Connection connection;
	
	/**
	 * Tables
	 */
	private final String tableName = "statistic";
	
	/**
	 * Constructor
	 * @param credential
	 */

	public MySQLConnector(Credential credential) {
		super(credential);
	}

	@Override
	public void connect() {
		StringBuilder builder = new StringBuilder()
	    .append(this.credential.getHost()).append(":")
		.append(this.credential.getPort())
		.append("/")
		.append(this.credential.getDatabaseName());
		try {
			Class.forName(driver);
			Connection connection = DriverManager.getConnection(builder.toString(), this.credential
					.getUser(), this.credential.getPassword());
			this.connection = connection;
			createTables();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	private void createTables() throws SQLException{
		PreparedStatement statment = connection.prepareStatement("CREATE TABLE IF NOT EXISTS " + this.tableName +
				"(uuid varchar(16), jsonString varchar(5000))");
		statment.execute();
	}
	
	public void update(UUID playerUUID, String jsonString) {
		try {
			PreparedStatement statement = connection.prepareStatement("REPLACE INTO " + this.tableName + " (uuid, jsonString) VALUES (?, ?)" );
			statement.setString(0, playerUUID.toString());
			statement.setString(1, jsonString);
			statement.executeQuery();
		} catch (Exception ex) { ex.printStackTrace(); }
	}
	
	public String getJsonString(UUID playerUUID) {
		try {
			PreparedStatement statment = connection.prepareStatement("SELECT * FROM " + this.tableName + " WHERE uuid = ?");
			statment.setString(0, playerUUID.toString());
			ResultSet set = statment.executeQuery();
			if(set.next()){
				String jsonString = set.getString("jsonString");
				return jsonString;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return "";
		}
		return "";
	}
	
	public boolean exists(UUID playerUUID){
		try {
			PreparedStatement statment = connection.prepareStatement("SELECT * FROM " + this.tableName + " WHERE uuid = ?");
			statment.setString(0, playerUUID.toString());
			ResultSet set = statment.executeQuery();
			if(set.next()){
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return true;
		}
		return false;
	}
	
	
	
	

	@Override
	public void disconnect() {
		try {
			if(!this.connection.isClosed()){
				this.connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	

}
