package eu.endcrystats.stats.database;

import lombok.Getter;

@Getter
public abstract class DatabaseConnector {
	
	public Credential credential;
	
	public DatabaseConnector(Credential credential) {
		this.credential = credential;
	}
	
	
	public abstract void connect();
	public abstract void disconnect();
	
	

}
