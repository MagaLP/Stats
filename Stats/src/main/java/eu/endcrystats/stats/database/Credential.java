package eu.endcrystats.stats.database;

import lombok.Getter;

@Getter
public class Credential {
	
	private String host;
	private int port;
	private String databaseName;
	private String user;
	private String password;
	
	public Credential(String host, int port, String databaseName, String user, String password) {
		this.host = host;
		this.port = port;
		this.databaseName = databaseName;
		this.user = user;
		this.password = password;
	}
	
	public String getHost() {
		return host;
	}
	
	public int getPort() {
		return port;
	}
	
	public String getDatabaseName() {
		return databaseName;
	}
	
	public String getUser() {
		return user;
	}
	
	public String getPassword() {
		return password;
	}
	

}
