package eu.endcrystats.stats.database;

import java.util.ArrayList;
import java.util.List;

import eu.endcrystats.stats.database.mysql.MySQLConnector;
import eu.endcrystats.stats.file.impl.DatabaseFile;
import lombok.Getter;

@Getter
public class Databases {
	
	private List<DatabaseConnector> connectors = new ArrayList<DatabaseConnector>();
    public final String statistikField = "stats";
    
    private MySQLConnector mySQLConnector;
    
    
    /**
     * Constructor
     * @param file
     */
	
	public Databases(DatabaseFile file) {
		loadConnectors(file);
	}
	
	private void loadConnectors(DatabaseFile file){
		String mySQLHost = file.getHost(file.getMysqlField());
		int mySQLPort = file.getPort(file.getMysqlField());
		String mySQLUser = file.getUser(file.getMysqlField());
		String mySQLPassword = file.getPassword(file.getMysqlField());
		
		MySQLConnector mySQLConnector = new MySQLConnector(
			new Credential(mySQLHost, mySQLPort,statistikField, mySQLUser, mySQLPassword)
			);
		this.mySQLConnector = mySQLConnector;
		this.connectors.add(mySQLConnector);
		
	}
	
	public void connect(){
		for(DatabaseConnector connector : this.connectors){
			connector.connect();
		}
	}
	
	public MySQLConnector getMySQLConnector() {
		return mySQLConnector;
	}
	
	public void disconnect() {
			
		for(DatabaseConnector connector : this.connectors){
			connector.disconnect();
		}
	}
	
}
