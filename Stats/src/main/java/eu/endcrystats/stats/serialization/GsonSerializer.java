package eu.endcrystats.stats.serialization;

import com.google.gson.JsonElement;

import eu.endcrystats.stats.utils.GsonUtil;

public interface GsonSerializer<T> {

	default JsonElement serialize(T object) {
		return GsonUtil.gson.toJsonTree(object);
	}
	
	JsonElement serialize();

}
