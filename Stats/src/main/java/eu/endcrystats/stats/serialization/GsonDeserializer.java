package eu.endcrystats.stats.serialization;

import java.lang.reflect.Field;

import eu.endcrystats.stats.utils.GsonUtil;

public interface GsonDeserializer<T> {

	@SuppressWarnings("unchecked")
	default void deserialize(T object, String jsonString) {
        Object deserialized = (T)GsonUtil.gson.fromJson(jsonString, object.getClass());
        
        for (Field field : object.getClass().getDeclaredFields()) {
        	field.setAccessible(true);
			try {
				Field valueField = deserialized.getClass().getDeclaredField(field.getName());
				valueField.setAccessible(true);
				Object value = valueField.get(deserialized);
	        	field.set(object, value);
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
    }
	
	void deserialize(String jsonFile);

}
