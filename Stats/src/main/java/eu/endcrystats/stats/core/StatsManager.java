package eu.endcrystats.stats.core;

import java.util.UUID;

import eu.endcrystats.stats.database.mysql.MySQLConnector;

public class StatsManager {

	private MySQLConnector mySQLConnector;
	
	public StatsManager(MySQLConnector connector) {
		this.mySQLConnector = connector;
	}
		
	public StatsPlayer getPlayerByUuid(UUID playerUuid) {
		StatsPlayer statsPlayer = new StatsPlayer();
		String jsonString = mySQLConnector.getJsonString(playerUuid);
		statsPlayer.deserialize(jsonString);
		return statsPlayer;
	}
	
	public void setGameStat(UUID playerUuid, String gameName, String valueName, String value) {
		StatsPlayer statsPlayer = this.getPlayerByUuid(playerUuid);
		if (statsPlayer == null) {
			statsPlayer = new StatsPlayer();
			statsPlayer.setPlayerUUID(playerUuid);
		}
		
		Stat gameStat = statsPlayer.getStatByName(gameName);
		if (gameStat == null) gameStat = new Stat();
		gameStat.getValues().put(valueName, value);
		
		mySQLConnector.update(playerUuid, statsPlayer.serialize().toString());
	}
	
	public Stat getStatByName(UUID playerUuid, String gameName) {
		return this.getPlayerByUuid(playerUuid).getStatByName(gameName);
	}
	
	public boolean hasGameStat(UUID playerUuid, String gameName) {
		return this.getPlayerByUuid(playerUuid).hasGameStat(gameName);
	}
	
	
}
