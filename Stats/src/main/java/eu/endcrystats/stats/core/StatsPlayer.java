package eu.endcrystats.stats.core;

import java.util.HashMap;
import java.util.UUID;

import com.google.gson.JsonElement;

import eu.endcrystats.stats.serialization.GsonDeserializer;
import eu.endcrystats.stats.serialization.GsonSerializer;

public class StatsPlayer implements GsonDeserializer<StatsPlayer>, GsonSerializer<StatsPlayer> {

	private UUID playerUUID;
	
	private HashMap<String, Stat> statsInformation = new HashMap<String, Stat>();

	public Stat getStatByName(String gameName) {
		return this.getStatsInformation().get(gameName);
	}
	
	public boolean hasGameStat(String gameName) {
		return this.getStatByName(gameName) == null;
	}
	
	@Override
	public JsonElement serialize() {
		return this.serialize(this);
	}
	
	@Override
	public void deserialize(String jsonString) {
		this.deserialize(this, jsonString);
	}
	
	public HashMap<String, Stat> getStatsInformation() {
		return statsInformation;
	}

	public void setStatsInformation(HashMap<String, Stat> statsInformation) {
		this.statsInformation = statsInformation;
	}

	public UUID getPlayerUUID() {
		return playerUUID;
	}

	public void setPlayerUUID(UUID playerUUID) {
		this.playerUUID = playerUUID;
	}
	
	
	
}
