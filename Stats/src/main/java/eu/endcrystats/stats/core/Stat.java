package eu.endcrystats.stats.core;

import java.util.HashMap;

public class Stat {

	private String gameName;
	
	private HashMap<String, String> values = new HashMap<String, String>();
	
	public String getStatValue(String valueName) { return this.values.get(valueName); }
	
	public void setStatValue(String valueName, String value) { this.values.put(valueName, valueName); }

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public HashMap<String, String> getValues() {
		return values;
	}

	public void setValues(HashMap<String, String> values) {
		this.values = values;
	}
	
}
